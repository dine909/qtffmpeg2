#include "scene.h"
#include "view.h"
#include <QApplication>


Scene::Scene(QObject *parent) :
    QGraphicsScene(parent),
    m_view(qobject_cast<View*>(parent))
{
    if(m_view){
        m_view->setScene(this);
    }

    setBackgroundBrush(QBrush(Qt::darkGreen));
    setFocus(Qt::ActiveWindowFocusReason);
}

View *Scene::view()
{
    return m_view;
}
