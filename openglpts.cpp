#include "openglpts.h"

#include <QDebug>

#include <GL/glx.h>
#include <GL/glu.h>
#include "GL/gl.h"
#include "GL/glext.h"
#include "GL/glxext.h"
#include <QOpenGLContext>
#include <QOpenGLFunctions>

#define RUNNING_FLAG    1
#define CLOCK_DIVISOR   1

u_int64_t lastCount;
u_int64_t urc=0;
bool swapset=false;

OpenGLPts::OpenGLPts() :
    Pts(&m_pts),
    m_pts(0.0)
{
    setTimeBase(AVRational({1*CLOCK_DIVISOR,60}));


}

void OpenGLPts::update()
{
    uint res;
    if(!swapset){
        res=glXSwapIntervalSGI(CLOCK_DIVISOR);
        swapset=true;
    }
    unsigned int count=0;
    res =glXWaitVideoSyncSGI(CLOCK_DIVISOR,0,&count);
//    res=glXGetVideoSyncSGI(&count);
    if(lastCount==count){
        qDebug()<<"OGL Repeat "  << lastCount << count;
//        count++;
    }
    if(lastCount+CLOCK_DIVISOR!=count){
        qDebug()<<"OGL Underrun " << ++urc << lastCount << count;
    }
    if(!res)
        setCount(count/CLOCK_DIVISOR);


    lastCount=count;
}
