#-------------------------------------------------
#
# Project created by QtCreator 2013-06-03T00:02:33
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtFFMpeg
TEMPLATE = app
linux:QMAKE_CXXFLAGS+=-D__STDC_CONSTANT_MACROS

debug: {
    QT+=testlib
    LIBS += -lgcov
    QMAKE_CXXFLAGS += -g -Wall --coverage -fprofile-arcs -ftest-coverage -fno-optimize-sibling-calls -O0
    QMAKE_LDFLAGS += -g -Wall --coverage -fprofile-arcs -ftest-coverage -fno-optimize-sibling-calls -O0
}

DEFINES += GL_GLEXT_PROTOTYPES GLX_GLXEXT_PROTOTYPES  GLX_NV_video_output=0

LIBS += -lavcodec -lavformat -lavutil -lswscale -lavfilter

LIBS += -L/opt/local/lib -L/usr/lib


linux:{
LIBS += -lGL -lXrandr
#QMAKE_CXXFLAGS+= -fpermissive
}


INCLUDEPATH +=  /opt/local/include

SOURCES += main.cpp \
    videoview.cpp

include(videoplayer.pri)
include(view.pri)
include(pts.pri)

HEADERS += \
    videoview.h

OTHER_FILES +=



