#ifndef QPIXMAPFRAME_H
#define QPIXMAPFRAME_H

#include <QTime>
#include <QPixmap>

class QPixmapFrame : public QPixmap
{

public:
    explicit QPixmapFrame();
    ~QPixmapFrame();
    QPixmapFrame(const QImage &image, const qreal fpts);
    QTime stamp;
    qreal pts;
signals:
    
public slots:

};

#endif // QPIXMAPFRAME_H
