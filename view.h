#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGLWidget>

#include "openglpts.h"

class Scene;
class View : public QGraphicsView, public OpenGLPts
{
    Q_OBJECT
    Scene *m_scene;
    QGLWidget *m_glwidget;
public:
    View(QWidget *parent = 0);
    ~View();
    void paintEvent(QPaintEvent *event);
signals:
    void updateItems();
};

#endif // MAINWINDOW_H
