#include "qpixmapframe.h"
#include <QDebug>


QPixmapFrame::QPixmapFrame():
    QPixmap(),
    pts(0.0)
{
//    stamp.start();
}

QPixmapFrame::~QPixmapFrame()
{
//    QPixmapCache::clear();
//    detach();

}

QPixmapFrame::QPixmapFrame(const QImage &image,const qreal fpts):
    QPixmap(QPixmap::fromImage(image)),
    pts(fpts)
{
    stamp.start();
}

