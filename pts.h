#ifndef PTS_H
#define PTS_H

#include <QWaitCondition>
#include <QMutex>

#include <stdint.h>
#include "ffmpegcommon.h"

#define RATIO_TO_TIME(t,n,d)((double)t*((double)n/(double)d))
#define TO_PTS(t,tb)((double)t*((double)tb->num/(double)tb->den))
#define FROM_PTS(t,tb)((u_int64_t)(((double)t/((double)tb->num/(double)tb->den))))

using namespace std;

class Pts
{

public:
    Pts(qreal *pts=0, AVRational *timeBase=0 );
    u_int64_t fromPts(const qreal Pts);
    u_int64_t fromPtsOffset(const qreal arg);
    qreal toPts(const u_int64_t count);
    qreal toPtsOffset(const u_int64_t arg);
    qreal pts();
    qreal *ptsPointer();
    qreal duration();

    void setCount(const u_int64_t arg);
    void setTimeBase(const AVRational arg);
    void setPtsOffset(const qreal arg);
    void setCountOffset(const u_int64_t arg);
    void setDuration(const qreal arg);
    void wait();
    qreal toPtsRatio(const u_int64_t arg);
protected:
    qreal m_pts;
    u_int64_t m_count;
    AVRational m_tb;
    AVRational *m_p_timeBase;
    qreal *m_p_pts;
    int64_t m_countOffset;
    qreal m_ptsOffset;
    QWaitCondition m_wait;
    QMutex m_waitLock;
    qreal m_duration;
};

#endif // PTS_H
