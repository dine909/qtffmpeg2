#ifndef OPENGLPTS_H
#define OPENGLPTS_H

#include <QThreadPool>
#include <QRunnable>

#include "pts.h"



#define RUNNING_FLAG 1

class OpenGLPts : public Pts
{
    double m_pts;
public:
    OpenGLPts();
    void update();
};

#endif // OPENGLPTS_H
