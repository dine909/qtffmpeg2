#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>


class View;
class Scene : public QGraphicsScene
{
    Q_OBJECT
    View *m_view;
public:
    explicit Scene(QObject *parent = 0);
    View *view();

signals:
    
public slots:
    
};

#endif // SCENE_H
