#include "avfilter.h"

#include <QDebug>
#include <QMutex>

#include "pts.h"
#include "ffmpegcommon.h"
#include <QtConcurrent/QtConcurrent>

QMutex bufferLock;
AVFrame *filtered_frame;

AvFilter::AvFilter()
{
    filtered_frame = av_frame_alloc();

}

AvFilter::~AvFilter()
{
    av_frame_free(&filtered_frame);
}
void AvFilter::filterVFrameIn(const AVFrame *frame){
    {
        QMutexLocker bufferLocker(&bufferLock);
        if (av_buffersrc_add_frame_flags(buffersrc_ctx, (AVFrame *)frame, AV_BUFFERSRC_FLAG_KEEP_REF) < 0) {
            av_log(NULL, AV_LOG_ERROR, "Error while feeding the filtergraph\n");
            qFatal("Error while feeding the filtergraph\n");
        }
    }
    //    QtConcurrent::run(this,&AvFilter::doFilterFrameOut);
    doFilterFrameOut();
}
void AvFilter::doFilterFrameOut(){
    int ret;

    {
        QMutexLocker bufferLocker(&bufferLock);
        ret = av_buffersink_get_frame(buffersink_ctx, filtered_frame);
    }
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
    {
        return;
    }
    else
    {
        setCount(filtered_frame->pts);
        QImage im=QImage((uchar*)filtered_frame->data[0], filtered_frame->width, filtered_frame->height, QImage::Format_ARGB32 );
        filterVFrameOut(QPixmapFrame(im,pts()));
        av_frame_unref(filtered_frame);
        doFilterFrameOut();
    }
}

int AvFilter::initFilters(QString filters_descr,AVCodecContext *dec_ctx)
{
    //    DPTR_D(FVDecoder);
    char args[512];
    int ret;
    AVFilter *buffersrc  = avfilter_get_by_name("buffer");
    AVFilter *buffersink = avfilter_get_by_name("buffersink");
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();
    enum AVPixelFormat pix_fmts[] = { AV_PIX_FMT_RGB24, AV_PIX_FMT_NONE };
    AVBufferSinkParams *buffersink_params;

    filter_graph = avfilter_graph_alloc();

    /* buffer video source: the decoded frames from the decoder will be inserted here. */
    snprintf(args, sizeof(args),
             "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
             dec_ctx->width, dec_ctx->height, dec_ctx->pix_fmt,
             dec_ctx->time_base.num, dec_ctx->time_base.den,
             dec_ctx->sample_aspect_ratio.num, dec_ctx->sample_aspect_ratio.den);
    qDebug()<<args;
    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                       args, NULL, filter_graph);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot create buffer source\n");
        return ret;
    }

    /* buffer video sink: to terminate the filter chain. */
    buffersink_params = av_buffersink_params_alloc();
    buffersink_params->pixel_fmts = pix_fmts;
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, buffersink_params, filter_graph);
    av_free(buffersink_params);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot create buffer sink\n");
        return ret;
    }

    /* Endpoints for the filter graph. */
    outputs->name       = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx    = 0;
    outputs->next       = NULL;

    inputs->name       = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;

    if ((ret = avfilter_graph_parse(filter_graph, QString("setpts=PTS-STARTPTS,"+ QString(filters_descr)).toStdString().c_str(),
                                    &inputs, &outputs, NULL)) < 0)
        return ret;

    if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
        return ret;

    buffersink_ctx->inputs[0]->time_base=dec_ctx->pkt_timebase;
    setTimeBase(buffersink_ctx->inputs[0]->time_base);

    return 0;
}
