#ifndef AVBUFFER_H
#define AVBUFFER_H

#include <QDebug>
#include <QMutex>
#include "pts.h"
#include "ffmpegcommon.h"
#include "qpixmapframe.h"


class AvFilter: public Pts
{
public:
    AvFilter();
    ~AvFilter();
    virtual void filterVFrameIn(const AVFrame *frame);
    virtual void filterVFrameOut(const QPixmapFrame frame)=0;
    void doFilterFrameOut();
protected:
    int initFilters(QString filters_descr, AVCodecContext *dec_ctx);
private:
    AVFilterGraph *filter_graph;
    AVFilterContext *buffersink_ctx;
    AVFilterContext *buffersrc_ctx;

};

#endif // AVBUFFER_H
