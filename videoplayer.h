#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QRunnable>
#include <QThreadPool>
#include <QQueue>

#include "qpixmapframe.h"
#include "avfilter.h"
#include "avdecoder.h"
#include "blockingqueue.h"

class View;
class VideoPlayer : public QObject, public QGraphicsPixmapItem,  public AvDecoder, public AvFilter
{
    Q_OBJECT
    const View *m_view;
    Pts *m_extPts;
    BlockingQueue<QPixmapFrame,QVector> vFrameQueue;
    qreal m_videoOffsetPts;
    QTime lastDisplay;
    Q_PROPERTY(QString filename READ filename WRITE setFilename)
    Q_PROPERTY(QString videoFilter READ videoFilter WRITE setVideoFilter)
    QString m_filename;
    QString m_videoFilter;
    QPixmapFrame currentPicture;
    QPixmapFrame lastPicture;
    bool started;
    QMutex bufferLock;
    Q_SLOT void display();
public:
    explicit VideoPlayer(QObject *parent = 0);
    ~VideoPlayer();
    bool running();

    static class _init
    {
        public :
        _init();
    }_initializer;

    void deleteFrame();
    virtual void setParent(QObject *);
    QString filename() const
    {
        return m_filename;
    }

    QString videoFilter() const
    {
        return m_videoFilter;
    }
    int bufferSize();
signals:
    
public slots:

    void start();
    void stop();

    void setFilename(QString arg)
    {
        m_filename = arg;
    }

    void setVideoFilter(QString arg)
    {
        m_videoFilter = arg;
    }

protected:
    void decoderVFrameOut(const AVFrame *frame);
    void filterVFrameOut(QPixmapFrame frame);
    Pts *extPts();
    bool ensureFrame();
    qreal getInPts(qreal offset=0.0);
};

#endif // VIDEOPLAYER_H
