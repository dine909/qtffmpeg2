#ifndef FFMPEG_COMMON_H
#define FFMPEG_COMMON_H

#include <QObject>

#include <math.h>
#ifndef PRId64
#define PRId64 "ld"
#endif

#ifndef PRId64
#define PRId64 "li"
#endif

#ifdef __cplusplus
extern "C"
{
#endif //__cplusplus

#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/channel_layout.h>
#include <libavutil/common.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/samplefmt.h>
#include <libswscale/swscale.h>

#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>
#include <libavutil/pixfmt.h>

#include <libavfilter/avfilter.h>
#include <libavfilter/avfiltergraph.h>
#include <libavfilter/avcodec.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>

#ifdef __cplusplus
}
#endif //__cplusplus


#endif // FFMPEGCOMMON_H
