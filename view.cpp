#include "view.h"
#include "scene.h"

#include <QTime>
#include <QApplication>

#include <GL/glx.h>
#include <GL/glu.h>
#include "GL/gl.h"
#include "GL/glext.h"
#include "GL/glxext.h"
#include <QOpenGLContext>
#include <QOpenGLFunctions>


QTime timer;

View::View(QWidget *parent):
      QGraphicsView(parent),
      OpenGLPts(),
      m_scene(new Scene(this)),
      m_glwidget(new QGLWidget(QGLFormat()))
{
//    showFullScreen();
    setViewport(m_glwidget);
    setFrameShape(NoFrame);
    setLineWidth(0);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setViewportUpdateMode(NoViewportUpdate);
    scene()->update();

    setWindowFlags(Qt::Window);
    setWindowState(Qt::WindowFullScreen|Qt::WindowActive);

    showFullScreen();
    setAlignment(Qt::AlignLeft|Qt::AlignTop);
}


View::~View()
{

}

//#define DEBUG_TIMER

void View::paintEvent(QPaintEvent *event)
{
#ifdef DEBUG_TIMER
    timer.start();

    QString result;
    result.append("start: "+   QString::number( timer.restart()));
#endif

    QGraphicsView::paintEvent(event);
#ifdef DEBUG_TIMER
    result.append(" paintEvent: "+  QString::number( timer.restart()));
#endif

    scene()->update();
#ifdef DEBUG_TIMER
    result.append(" scene update: "+  QString::number( timer.restart()));
#endif

    if(pts()!=0.0)
        emit updateItems();
#ifdef DEBUG_TIMER
    result.append(" update items: "+  QString::number( timer.restart()));
#endif

    OpenGLPts::update();
#ifdef DEBUG_TIMER
    result.append(" ogl pts update: "+  QString::number( timer.restart()));
    qDebug() << result;
#endif


}
