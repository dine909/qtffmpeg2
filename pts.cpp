#include "pts.h"
#include "math.h"

Pts::Pts(qreal *pts, AVRational *timeBase) :
    m_pts(0.0),
    m_count(0),
    m_tb(AVRational({1,1})),
    m_p_timeBase(timeBase?timeBase:&m_tb),
    m_p_pts(pts?pts:&m_pts),
    m_countOffset(0),
    m_ptsOffset(0.0),
    m_duration(0.0)
{
}

void Pts::setCount(const u_int64_t count)
{
    m_count=count;
    *m_p_pts=((qreal)(count+m_countOffset))*((qreal)m_p_timeBase->num/(qreal)m_p_timeBase->den);
    QMutexLocker locker(&m_waitLock);
    m_wait.wakeAll();
}

void Pts::wait(){
    QMutexLocker locker(&m_waitLock);
    m_wait.wait(&m_waitLock);
}

double Pts::pts()
{
    return (*m_p_pts)+m_ptsOffset;
}

double *Pts::ptsPointer()
{
    return m_p_pts;
}

qreal Pts::duration()
{
    return m_duration;
}

void Pts::setTimeBase(const AVRational arg)
{
    m_tb=arg;
    m_p_timeBase=&m_tb;
}

void Pts::setPtsOffset(const qreal arg)
{
    m_ptsOffset=arg;
}

void Pts::setCountOffset(const u_int64_t arg)
{
    m_countOffset=arg;
}

void Pts::setDuration(const qreal arg)
{
    m_duration=arg;
}

qreal Pts::toPtsRatio(const u_int64_t arg)
{
    return ((qreal)(arg)*((qreal)m_p_timeBase->num/(qreal)m_p_timeBase->den));
}

qreal Pts::toPts(const u_int64_t arg)
{
    return ((qreal)(arg+m_countOffset)*((qreal)m_p_timeBase->num/(qreal)m_p_timeBase->den));
}
qreal Pts::toPtsOffset(const u_int64_t arg)
{
    return toPts(arg)+m_ptsOffset;
}
u_int64_t Pts::fromPts(const qreal arg)
{
    return floor((qreal)(arg-m_ptsOffset)/((qreal)m_p_timeBase->num/(qreal)m_p_timeBase->den));
}
u_int64_t Pts::fromPtsOffset(const qreal arg)
{
    return fromPts(arg)-m_countOffset;
}
