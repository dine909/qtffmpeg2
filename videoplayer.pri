
SOURCES += videoplayer.cpp  \
     openglpts.cpp \
    avdecoder.cpp \
    avfilter.cpp \
    qpixmapframe.cpp

HEADERS  += \
    videoplayer.h \
    ffmpegcommon.h \
    openglpts.h \
    avdecoder.h \
    avfilter.h \
    qpixmapframe.h \
    blockingqueue.h


