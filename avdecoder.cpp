#include "avdecoder.h"
#include <QThread>

QMutex AvDecoder::ocmutex;



QMutex seekMutex;
QWaitCondition seekWait;

QMutex pauseMutex;
QWaitCondition pauseWaitCondition;

uint64_t global_video_pkt_pts = AV_NOPTS_VALUE;

AvDecoder::AvDecoder() :
    m_fmt_ctx(0),
    m_dec_ctx(0)

{
    AvDecoder::ocmutex.lock();
    avcodec_register_all();
    av_register_all();
    avfilter_register_all();
    avformat_network_init();
    AvDecoder::ocmutex.unlock();
    setAutoDelete(false);

}

void AvDecoder::seekPts(qreal pts){
    seekts=pts;
    seek=FV_SEEK_SEEK;
}


int AvDecoder::openInputFile(const char *filename)
{
    //    DPTR_D(FVDecoder);
    int ret;
    AVCodec *dec=0;

    if ((ret = avformat_open_input(&m_fmt_ctx, filename, NULL, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
        return ret;
    }

    if ((ret = avformat_find_stream_info(m_fmt_ctx, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
        return ret;
    }
    //    av_dump_format(m_fmt_ctx, 0, filename, 0);
    /* select the video stream */
    ret = av_find_best_stream(m_fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
        return ret;
    }
    video_stream_index = ret;
    m_dec_ctx = m_fmt_ctx->streams[video_stream_index]->codec;

    /* init the video decoder */
    if ((ret = avcodec_open2(m_dec_ctx, dec, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
        return ret;
    }
    qDebug()<<"dec init" << m_dec_ctx << m_fmt_ctx;

    setDuration((qreal)m_fmt_ctx->duration/(qreal)AV_TIME_BASE);
    return 0;
}

AVCodecContext *AvDecoder::dec_ctx()
{
    return m_dec_ctx;
}

QMutex *AvDecoder::quitMutex()
{
    return &quitmutex;
}

void AvDecoder::run(){


    //    DPTR_D(FVDecoder);
    QMutexLocker lockrun(&quitmutex);
    //    Q_UNUSED(lockrun)

    int ret;
#define STATE_VAR state
#define STATE_ERR_VAR state_error
#define STATE_ERR_STATE FV_ERRORED

#define SET_STATE(s) STATE_VAR=(FV_STATE)(s);
#define NEXT_STATE SET_STATE(STATE_VAR+(FV_STATE)1);
#define GO_STATE(s) SET_STATE(s);goto s##_jmp;break;
#define GO_ERR_STATE(s) SET_STATE(STATE_ERR_STATE);STATE_ERR_VAR=s;break;
#define SET_ERR_STATE(s) SET_STATE(STATE_ERR_STATE);STATE_ERR_VAR=s;
#define DO_STATE(s) case s: s##_jmp:
#define END_STATE break;
#define SPIN break;

    FV_STATE state=FV_OPEN;
    AVPacket packet;
    AVFrame *frame = av_frame_alloc();
    int got_frame;
    bool nodata=false;
    bool dirty=false;
    qreal cpts=-1.0;

    qreal frameDuration=((qreal)m_dec_ctx->time_base.num/(qreal)m_dec_ctx->time_base.den);
    frameDuration*=(qreal)m_dec_ctx->ticks_per_frame;

    while(running()){
        switch (state) {
        DO_STATE(FV_OPEN){
            seek=FV_SEEK_NO_SEEK;
            avcodec_get_frame_defaults(frame);
            NEXT_STATE;
        }
        END_STATE;

        DO_STATE(FV_STARTED){
            setTimeBase(m_dec_ctx->pkt_timebase);
            setPtsOffset((qreal)(-((qreal)m_fmt_ctx->start_time/(qreal)AV_TIME_BASE)));
            NEXT_STATE;
        }
        END_STATE;

        DO_STATE(FV_SEEK){
            qreal inpts=getInPts(BUFFER_TIME);

            if(cpts!=-1.0){
                if(fabs(cpts-inpts)>MAX_SEEK_OFFSET){
                    seekPts(inpts);
                    cpts=-1.0;
                }
                else if (cpts>inpts)
                {
                    QThread::currentThread()->msleep(10);
                    END_STATE;
                }
            }

            if(seek&FV_SEEK_SEEK){
                nodata=false;

                //quantize
                seekts=floor(seekts/frameDuration);
                seekts*=frameDuration;
                int sflags = AVSEEK_FLAG_FRAME;
                int64_t tsa=0;
                 if(seekts==0.0){
                     sflags|=AVSEEK_FLAG_BACKWARD;
                 }
//                if(seekts!=0.0){
                    tsa=floor((seekts)*(qreal)AV_TIME_BASE);
                    if(tsa<0){
                        GO_STATE(FV_ENDED);
                    }
                    tsa+=m_fmt_ctx->start_time;
//                }
                packet.data=NULL;
                packet.size=0;

                avcodec_flush_buffers(m_dec_ctx);
                if(av_seek_frame(m_fmt_ctx,-1,tsa,sflags)>=0){
                    qDebug("seeking to %.2f - %ud",seekts,tsa);
                }else{
                    qDebug()<<"cannot seek to "   << seekts << tsa;
                }
                seek=FV_SEEK_NO_SEEK;
                //                QMutexLocker seekLocker(&seekMutex);
                //                seekWait.wakeAll();

            }

            GO_STATE(FV_DECODE);
        }
        END_STATE;

        DO_STATE(FV_DECODE){
            {
                //                QMutexLocker lockopen(&ocmutex);
                if ((ret = av_read_frame(m_fmt_ctx, &packet)) < 0){
                    nodata=true;
                    packet.data=NULL;
                    packet.size=0;
                    //                    GO_STATE(FV_ENDED)
                }
            }
            if (packet.stream_index == video_stream_index) {
                got_frame = 0;
                qreal ppts=toPtsRatio(packet.dts);
                //                if(fabs(ppts-pts())<=MAX_DECODE_OFFSET)
                {
                    ret = avcodec_decode_video2(m_dec_ctx, frame, &got_frame, &packet);
                    if (ret < 0) {
                        qFatal("Error decoding video\n");
                    }else if(got_frame){

                        frame->pts=av_frame_get_best_effort_timestamp(frame);
                        setCount(frame->pts);
                        cpts=pts();
                        decoderVFrameOut(frame);
                        av_frame_unref(frame);
                        SET_STATE(FV_SEEK);
                     }else if(nodata){
                        SET_STATE(FV_ENDED)
                    }
                }
                //                else{
                //                    SET_STATE(FV_SEEK)
                //                }
            }

            av_free_packet(&packet);
        }
        END_STATE;

        DO_STATE(FV_ENDED){
            //            seekPts(0.0);
            //            qDebug() << "restaring";
            //            decoderVFrameOut(0);
            QThread::currentThread()->msleep(10);
            GO_STATE(FV_SEEK);
        }
        END_STATE;



        default:
            goto exitstates;
            break;
        }
    }
exitstates:

    {
        QMutexLocker lockopen(&ocmutex);

        if (m_dec_ctx){
            avcodec_close(m_dec_ctx);
            m_dec_ctx=0;
        }
        if(m_fmt_ctx)
        {
            avformat_close_input(&m_fmt_ctx);
            m_fmt_ctx=0;
        }
    }
    av_frame_free(&frame);


    //    return;
}
