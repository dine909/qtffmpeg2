#ifndef AVDECODER_H
#define AVDECODER_H

#include <QDebug>
#include <QMutex>
#include <QRunnable>

#include "pts.h"
#include "ffmpegcommon.h"
#include "qpixmapframe.h"

#define MAX_SEEK_OFFSET 3.0
#define MAX_DECODE_OFFSET 0.5
#define BUFFER_TIME 0.20

class AvDecoder: public Pts,public QRunnable
{

public:
    enum FV_STATE {
        FV_ERRORED=-1,
        FV_OPEN,
        FV_STARTED,
        FV_SEEK,
        FV_DECODE,
        FV_ENDED
    };
    enum FV_SEEK_FLAGS {
        FV_SEEK_NO_SEEK=0,
        FV_SEEK_SEEK=0x1
    };
    static QMutex ocmutex;

    virtual bool running()=0;
    virtual void decoderVFrameOut(const AVFrame *frame)=0;
    virtual qreal getInPts(qreal offset=0.0)=0;

    virtual Pts *extPts()=0;
    AvDecoder();
    void run();
    AVCodecContext *dec_ctx();
    QMutex* quitMutex();
    void seekPts(qreal pts);
protected:
    int openInputFile(const char *filename);
    AVFormatContext *m_fmt_ctx;
    AVCodecContext *m_dec_ctx;
    int video_stream_index;
    QMutex quitmutex;
private:
    FV_SEEK_FLAGS seek;
    qreal seekts;

    //    qreal extPtsOffset;
};

#endif // AVDECODER_H
