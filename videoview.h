#ifndef VIDEOVIEW_H
#define VIDEOVIEW_H

#include "view.h"
#include "videoplayer.h"

class VideoView : public View
{
    Q_OBJECT
    VideoPlayer m_player;

public:
    explicit VideoView(QWidget *parent = 0);
    void keyPressEvent(QKeyEvent *event);

signals:
    
public slots:
    
};

#endif // VIDEOVIEW_H
