#include "videoplayer.h"
#include "scene.h"
#include "view.h"
#include "QtConcurrent/QtConcurrent"
#include <QPointer>

#define RUNNING_FLAG started
#define MAX_PTS_OFFSET 1.0 + MAX_SEEK_OFFSET

static QThreadPool decoderPool;

VideoPlayer::_init VideoPlayer::_initializer;

VideoPlayer::_init::_init()
{
    decoderPool.setMaxThreadCount(32);
}

VideoPlayer::VideoPlayer(QObject *parent) :
    QObject(parent),
    QGraphicsPixmapItem(),
    m_videoOffsetPts(0.0),
    started(false),
    m_filename(""),
    m_videoFilter("null"),
    currentPicture()
{

    //    vFrameQueue.setThreshold(10000);
    vFrameQueue.setBlocking(true);

}

VideoPlayer::~VideoPlayer()
{

}

void VideoPlayer::start(){
    vFrameQueue.setCapacity(10);
    if(started)return;
    if(m_view) connect(m_view,SIGNAL(updateItems()),this,SLOT(display()),Qt::DirectConnection);

    {
        int ret;
        {
            QMutexLocker lockopen(&ocmutex);
            if ((ret = openInputFile(m_filename.toStdString().c_str())) < 0){
                qFatal("could not open file");
            }
        }

        if ((ret = initFilters(QString("format=rgb32,"+m_videoFilter).toStdString().c_str(),dec_ctx())) < 0){
            qFatal("could not create filters");
        }
    }
    //    m_extPts->wait();

    m_videoOffsetPts=m_extPts->pts();
    QThreadPool::globalInstance()->start(this,20);
    started=true;
}

void VideoPlayer::stop()
{
    if(!started)return;
    if(m_view)disconnect(m_view,SIGNAL(updateItems()),this,SLOT(display()));
    started=false;
    quitmutex.lock();
    quitmutex.unlock();

    vFrameQueue.setCapacity(0);
    vFrameQueue.clear();
}

qreal VideoPlayer::getInPts(qreal offset){
    qreal epts=m_extPts->pts()+offset;
    return  fmod( epts,AvDecoder::m_duration);
}

void VideoPlayer::setParent(QObject * arg)
{
    QObject::setParent(arg);
    m_view=qobject_cast<View*>(arg);
    m_extPts=(Pts*)m_view;
    if(m_view){
        m_view->scene()->addItem(this);
    }
}

int VideoPlayer::bufferSize()
{
    return vFrameQueue.size();
}


bool VideoPlayer::ensureFrame(){

    if(!currentPicture.stamp.isValid()&&!vFrameQueue.isEmpty()){
        currentPicture=vFrameQueue.take();
    }
    return currentPicture.stamp.isValid();
}

void VideoPlayer::deleteFrame()
{

    if(currentPicture.stamp.isValid()){
        currentPicture.stamp= QTime();
    }

}

void VideoPlayer::display()
{
    if(!RUNNING_FLAG) return;
    qreal inpts=getInPts();

tryagain:

    if(ensureFrame())
    {
        const qreal picpts=currentPicture.pts;
        //seek ?
        const qreal vdu=m_extPts->toPts(1);


        if(fabs(inpts-picpts)>MAX_PTS_OFFSET){ // Is picture within playback offset window?
            if((!lastDisplay.isValid()) // is this a continuation of untimely frames?
                    ||(currentPicture.stamp.elapsed())>MAX_PTS_OFFSET*1000){ // or is this frame offset and stale?
                qDebug("frame is out by more than %.2fs, frame PTS %.2fs - current PTS %.2fs",MAX_PTS_OFFSET,picpts,inpts);

                deleteFrame();
                lastDisplay=QTime(); // mark previous frame as untimley;
                goto tryagain; // keep trying in this vdu period
            }
            return; // give this frame a chance as previous frames were ok, spin vdu period
        }
        // is picture in future?
//        if(picpts>inpts)

        //is picture within a vdu frame?
        qreal diff=picpts-inpts; // frame pts delta
        diff=ceil(diff/vdu); // quantize picture pts into vdu refresh cycle steps
        if(diff==0.0 ){// is it time to display this frame?
            lastDisplay.start(); // mark previous frame as timley;
            setPixmap(currentPicture); // display picture
            deleteFrame();
        }else{
            return; // wait for frame, spin vdu period
//            qDebug("frame out, frame PTS %.2fs - current PTS %.2fs (diff %.0f frames)",picpts,inpts,diff);
//            deleteFrame();
//            goto tryagain; // keep trying in this vdu period
        }

    }else{
        //        qDebug("no frame for %.2fs, buffer empty - well this is embarassing.. ",inpts);

    }
    //        qDebug()<<"pts: "<< pts ;
}

void VideoPlayer::decoderVFrameOut(const AVFrame *frame)
{
    filterVFrameIn(frame);
}

void VideoPlayer::filterVFrameOut(QPixmapFrame frame)
{
    if(started){
        QMutexLocker bufferLocker(&bufferLock);
        vFrameQueue.put(frame);
        return;
        while(!vFrameQueue.putWait(frame,1))
        {
            if(!started){

                frame=QPixmapFrame();
//                delete frame;
//                frame=0;
                return;
            }else{
                //                QThread::currentThread()->msleep(sl*2);
            }
        }
        //        QThread::currentThread()->msleep(sl);
    }
    else{
//        delete frame;
//        frame=0;

          frame=QPixmapFrame();
    }
}

bool VideoPlayer::running()
{
    return RUNNING_FLAG;
}

Pts *VideoPlayer::extPts()
{
    return m_extPts;
}

