#include "videoview.h"
#include <QApplication>
#include <QKeyEvent>
#include <QPixmapCache>

VideoView::VideoView(QWidget *parent) :
    View(parent)
{
    m_player.setParent(this);
    m_player.setX(0);
    m_player.setY(0);

    m_player.setFilename("/sources/media/GH3 1080 60P Aerial slo-mo.mp4");
    m_player.start();

}


void VideoView::keyPressEvent(QKeyEvent *event){
    uint key = event->key();
    switch(key){
    case Qt::Key_Escape:
        QApplication::exit(0);
        break;
    case Qt::Key_D:
        qDebug()<<m_player.bufferSize();
        break;
    case Qt::Key_C:

        QPixmapCache::clear();
        break;
    case Qt::Key_Space:
         if(m_player.running())
         {
             m_player.stop();
         }
         else
         {
             m_player.start();
         }
         break;
     }

}
